##
Guide for running Data file processing automation
##
install the following packages
bokeh==1.4.0
pandas==0.25.3
psycopg2==2.8.2

##
install postgresql 12

##
run database script

##
run orchestrator.py
##
to schedule job to run monthly, add the following cron settings
0 0 1 * * /home/[user]/fileprocessingautomation/orchestrator.py
