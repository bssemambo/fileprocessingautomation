import smtplib
import ssl

import psycopg2


class db_conn():
    def __init__(self, pshost, psuser, pspassword, psdatabase, psport):
        """
         postgres_db_conn constructor
         @param pshost: postgreSQL database server to which the connection will be made
         @param psuser: database user connecting to the @psdatabase on @pshost
         @param pspassword: @psuser database server password
         @param psdatabase: the database that is to be used
         @param psport: @pshost port
        """
        try:
            self.conn = psycopg2.connect(host=pshost,
                                         user=psuser,
                                         password=pspassword,
                                         database=psdatabase,
                                         port=psport)

            self.conn.set_client_encoding('utf-8')

            self.cursor = self.conn.cursor()

        except Exception as errmsg:
            print(errmsg)
            raise


class db_ops():
    def __init__(self, parser, cursor):
        self.insert_lst = []
        self.parser = parser
        self.cursor = cursor

    def create_insert_list(self, record):

        rows = ()
        for each_field in record:
            if not record[each_field]:
                rows += (None,)
            else:
                rows += (record[each_field],)

        self.insert_lst.append(rows)

    def load_to_database(self, columns_str, data_str, data, table_name):
        sql = str("""INSERT INTO """+table_name+"""
                 ("""+columns_str+""")
                 values
                 ("""+data_str+""")""")

        self.cursor.executemany(sql, data)

    def load_stats(self, job_id, stats):
        for each_stat in stats:
            cursor.execute("""INSERT INTO job_stats
                            (job_id, stat_desc, stat_value)
                            VALUES
                            (%s, %s, %s)""", (submission_id, each_stat, stats[each_stat]))


class File_Parser():
    def __init__(self, cursor):
        self.lookupdic = {}
        self.cursor = cursor
        self.record = {}
        self.table_values = []
        self.fields = {1: 'job_id',
                       2: 'loan_number',
                       3: 'region_id',
                       4: 'country_id',
                       6: 'borrower_id',
                       7: 'guarantor_country_id',
                       9: 'loan_type_id',
                       10: 'loan_status_id',
                       11: 'interest_rate',
                       12: 'currency_of_commitment',
                       13: 'project_id',
                       15: 'original_principal_amount',
                       16: 'cancelled_amount',
                       17: 'undisbursed_amount',
                       18: 'disbursed_amount',
                       19: 'repaid_to_ibrd',
                       20: 'due_to_ibrd',
                       21: 'exchange_adjustment',
                       22: 'borrowers_obligation',
                       23: 'sold_3rd_party',
                       24: 'repaid_3rd_party',
                       25: 'due_3rd_party',
                       26: 'loans_held',
                       27: 'first_repayment_date',
                       28: 'last_repayment_date',
                       29: 'agreement_signing_date',
                       30: 'board_approval_date',
                       31: 'effective_date_most_recent',
                       32: 'closed_date_most_recent',
                       33: 'last_disbursement_date'
                       }

    def load_lookups(self):
        self.cursor.execute(
            """select country_code, country_name, region_id from countries""")
        for country_code, country_name, region_id in self.cursor.fetchall():
            self.lookupdic['countries'][country_code] = [
                country_name, region_id]

        self.cursor.execute(
            """select description, lable, value from job_status""")
        for description, lable, value in self.cursor.fetchall():
            self.lookupdic['job_status'][value] = [description, lable]

        self.cursor.execute("""select id, loan_status_desc from loan_status""")
        for id, loan_status_desc in self.cursor.fetchall():
            self.lookupdic['loan_status'][loan_status_desc] = id

        self.cursor.execute("""select id, loan_type_desc from loan_type""")
        for id, loan_type_desc in self.cursor.fetchall():
            self.lookupdic['loan_type'][loan_type_desc] = id

        self.cursor.execute("""select id, region_name from regions""")
        for id, region_name in self.cursor.fetchall():
            self.lookupdic['regions'][region_name] = id

        self.cursor.execute("""select id, borrower_name from borrowers""")
        for id, borrower_name in self.cursor.fetchall():
            self.lookupdic['borrowers'][borrower_name] = id

    def parse_csv(self, entry, job_id):
        cnt = 1
        for field in entry:
            try:
                if cnt == 1:
                    self.record[self.fields[cnt]] = job_id
                    self.table_values.append('%s')
                elif cnt == 3:
                    self.record[self.fields[cnt]
                                ] = self.lookupdic['regions'][field]
                    self.table_values.append('%s')
                elif cnt == 4:
                    self.record[self.fields[cnt]
                                ] = self.lookupdic['countries'][field]
                    self.table_values.append('%s')
                elif cnt == 6:
                    self.record[self.fields[cnt]
                                ] = self.lookupdic['borrowers'][field]
                    self.table_values.append('%s')
                elif cnt == 7:
                    self.record[self.fields[cnt]
                                ] = self.lookupdic['countries'][field]
                    self.table_values.append('%s')
                elif cnt == 9:
                    self.record[self.fields[cnt]
                                ] = self.lookupdic['loan_type'][field]
                    self.table_values.append('%s')
                elif cnt == 10:
                    self.record[self.fields[cnt]
                                ] = self.lookupdic['loan_status'][field]
                    self.table_values.append('%s')

                else:
                    self.record[self.fields[cnt]] = field
                    self.table_values.append('%s')
                cnt += 1
            except KeyError:
                cnt += 1
                pass

class Email():
    def __init__(self, port, smtp_server, sender_email , password):
        self.port = port
        self.smtp_server = "smtp.gmail.com"
        self.sender_email = sender_email
        self.receiver_email = []
        self.password = password
        self.message = None

    def send_email(self):
        context = ssl.create_default_context()
        with smtplib.SMTP(smtp_server, port) as server:
            server.ehlo() 
            server.starttls(context=context)
            server.ehlo()
            server.login(sender_email, password)
            for receiver in self.receiver_email:
                server.sendmail(self.sender_email, receiver, message)
