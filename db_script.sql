--
-- PostgreSQL database dump
--

-- Dumped from database version 12.1
-- Dumped by pg_dump version 12.1

-- Started on 2020-01-15 09:09:21

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 16384)
-- Name: adminpack; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;


--
-- TOC entry 2919 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION adminpack; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';


--
-- TOC entry 224 (class 1255 OID 16464)
-- Name: trig_logger(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.trig_logger() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
		IF TG_OP = 'INSERT' THEN
            NEW.CREATED_BY := CURRENT_USER;
            NEW.DATE_CREATED := CURRENT_TIMESTAMP;
            RETURN NEW;
		ELSIF TG_OP = 'UPDATE' THEN
            NEW.MODIFIED_BY := CURRENT_USER;
            NEW.DATE_MODIFIED := CURRENT_TIMESTAMP;
            RETURN NEW;
        ELSIF TG_OP = 'DELETE' THEN
            RETURN OLD;
		END IF;
			
	END;
$$;


ALTER FUNCTION public.trig_logger() OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 214 (class 1259 OID 16425)
-- Name: borrowers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.borrowers (
    id integer NOT NULL,
    borrower_name character varying(100) NOT NULL,
    date_created timestamp without time zone,
    created_by character varying(100) NOT NULL,
    date_modified timestamp without time zone,
    modified_by character varying(100)
);


ALTER TABLE public.borrowers OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 16395)
-- Name: countries; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.countries (
    id integer NOT NULL,
    country_code character varying(100) NOT NULL,
    country_name character varying(100) NOT NULL,
    region_id integer,
    date_created date,
    created_by character varying(100) NOT NULL,
    date_modified date,
    modified_by character varying(100)
);


ALTER TABLE public.countries OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 16439)
-- Name: job_stats; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.job_stats (
    id integer NOT NULL,
    job_id integer,
    stat_desc character varying(100) NOT NULL,
    stat_value character varying(100) NOT NULL,
    date_created timestamp without time zone,
    created_by character varying(100) NOT NULL,
    date_modified timestamp without time zone,
    modified_by character varying(100)
);


ALTER TABLE public.job_stats OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 16493)
-- Name: job_status; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.job_status (
    id integer NOT NULL,
    description character varying(100) NOT NULL,
    lable character varying(100) NOT NULL,
    value smallint NOT NULL,
    date_created timestamp without time zone,
    created_by character varying(100) NOT NULL,
    date_modified timestamp without time zone,
    modified_by character varying(100)
);


ALTER TABLE public.job_status OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 16553)
-- Name: jobs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.jobs (
    id integer NOT NULL,
    job_name character varying(100) NOT NULL,
    job_status smallint NOT NULL,
    start_time timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    end_time timestamp without time zone,
    file_name text,
    date_created timestamp without time zone,
    created_by character varying(100) NOT NULL,
    date_modified timestamp without time zone,
    modified_by character varying(100)
);


ALTER TABLE public.jobs OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 16498)
-- Name: loan_accounts; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.loan_accounts (
    id integer NOT NULL,
    job_id integer,
    loan_number character varying(10),
    region_id integer,
    country_id integer,
    borrower_id integer,
    guarantor_country_id integer,
    loan_type_id integer,
    loan_status_id integer,
    interest_rate numeric(5,2),
    currency_of_commitment character varying(5),
    project_id integer,
    original_principal_amount numeric(25,2),
    cancelled_amount numeric(25,2),
    undisbursed_amount numeric(25,2),
    disbursed_amount numeric(25,2),
    repaid_to_ibrd numeric(25,2),
    due_to_ibrd numeric(25,2),
    exchange_adjustment numeric(25,2),
    borrowers_obligation numeric(25,2),
    sold_3rd_party numeric(25,2),
    repaid_3rd_party numeric(25,2),
    due_3rd_party numeric(25,2),
    loans_held numeric(25,2),
    first_repayment_date date,
    last_repayment_date date,
    agreement_signing_date date,
    board_approval_date date,
    effective_date_most_recent date,
    closed_date_most_recent date,
    last_disbursement_datet date,
    date_created timestamp without time zone,
    created_by character varying(100) NOT NULL,
    date_modified timestamp without time zone,
    modified_by character varying(100) NOT NULL
);


ALTER TABLE public.loan_accounts OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 16419)
-- Name: loan_status; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.loan_status (
    id integer NOT NULL,
    loan_status_desc character varying(100) NOT NULL,
    date_created timestamp without time zone,
    created_by character varying(100) NOT NULL,
    date_modified timestamp without time zone,
    modified_by character varying(100)
);


ALTER TABLE public.loan_status OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 16411)
-- Name: table_loan_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.table_loan_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.table_loan_type_id_seq OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 16413)
-- Name: loan_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.loan_type (
    id integer DEFAULT nextval('public.table_loan_type_id_seq'::regclass) NOT NULL,
    loan_type_desc character varying(100) NOT NULL,
    date_created timestamp without time zone,
    created_by character varying(100) NOT NULL,
    date_modified timestamp without time zone,
    modified_by character varying(100)
);


ALTER TABLE public.loan_type OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 16407)
-- Name: projects; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.projects (
    id integer NOT NULL,
    project_id character varying(50) NOT NULL,
    project_name character varying(100) NOT NULL,
    date_created timestamp without time zone,
    created_by character varying(100) NOT NULL,
    date_modified timestamp without time zone,
    modified_by character varying(100)
);


ALTER TABLE public.projects OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 16401)
-- Name: regions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.regions (
    id integer NOT NULL,
    region_name character varying(100) NOT NULL,
    date_created timestamp without time zone,
    created_by character varying(100) NOT NULL,
    date_modified timestamp without time zone,
    modified_by character varying(100)
);


ALTER TABLE public.regions OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 16423)
-- Name: table_borrowers_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.table_borrowers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.table_borrowers_id_seq OWNER TO postgres;

--
-- TOC entry 2920 (class 0 OID 0)
-- Dependencies: 213
-- Name: table_borrowers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.table_borrowers_id_seq OWNED BY public.borrowers.id;


--
-- TOC entry 203 (class 1259 OID 16393)
-- Name: table_countries_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.table_countries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.table_countries_id_seq OWNER TO postgres;

--
-- TOC entry 2921 (class 0 OID 0)
-- Dependencies: 203
-- Name: table_countries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.table_countries_id_seq OWNED BY public.countries.id;


--
-- TOC entry 216 (class 1259 OID 16437)
-- Name: table_job_stats_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.table_job_stats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.table_job_stats_id_seq OWNER TO postgres;

--
-- TOC entry 2922 (class 0 OID 0)
-- Dependencies: 216
-- Name: table_job_stats_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.table_job_stats_id_seq OWNED BY public.job_stats.id;


--
-- TOC entry 219 (class 1259 OID 16491)
-- Name: table_job_status_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.table_job_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.table_job_status_id_seq OWNER TO postgres;

--
-- TOC entry 2923 (class 0 OID 0)
-- Dependencies: 219
-- Name: table_job_status_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.table_job_status_id_seq OWNED BY public.job_status.id;


--
-- TOC entry 222 (class 1259 OID 16551)
-- Name: table_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.table_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.table_jobs_id_seq OWNER TO postgres;

--
-- TOC entry 2924 (class 0 OID 0)
-- Dependencies: 222
-- Name: table_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.table_jobs_id_seq OWNED BY public.jobs.id;


--
-- TOC entry 218 (class 1259 OID 16458)
-- Name: table_loan_accounts_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.table_loan_accounts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.table_loan_accounts_id_seq OWNER TO postgres;

--
-- TOC entry 2925 (class 0 OID 0)
-- Dependencies: 218
-- Name: table_loan_accounts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.table_loan_accounts_id_seq OWNED BY public.loan_accounts.id;


--
-- TOC entry 215 (class 1259 OID 16435)
-- Name: table_loan_stats_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.table_loan_stats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.table_loan_stats_id_seq OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 16417)
-- Name: table_loan_status_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.table_loan_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.table_loan_status_id_seq OWNER TO postgres;

--
-- TOC entry 2926 (class 0 OID 0)
-- Dependencies: 211
-- Name: table_loan_status_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.table_loan_status_id_seq OWNED BY public.loan_status.id;


--
-- TOC entry 207 (class 1259 OID 16405)
-- Name: table_projects_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.table_projects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.table_projects_id_seq OWNER TO postgres;

--
-- TOC entry 2927 (class 0 OID 0)
-- Dependencies: 207
-- Name: table_projects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.table_projects_id_seq OWNED BY public.projects.id;


--
-- TOC entry 205 (class 1259 OID 16399)
-- Name: table_regions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.table_regions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.table_regions_id_seq OWNER TO postgres;

--
-- TOC entry 2928 (class 0 OID 0)
-- Dependencies: 205
-- Name: table_regions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.table_regions_id_seq OWNED BY public.regions.id;


--
-- TOC entry 2751 (class 2604 OID 16428)
-- Name: borrowers id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.borrowers ALTER COLUMN id SET DEFAULT nextval('public.table_borrowers_id_seq'::regclass);


--
-- TOC entry 2746 (class 2604 OID 16398)
-- Name: countries id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.countries ALTER COLUMN id SET DEFAULT nextval('public.table_countries_id_seq'::regclass);


--
-- TOC entry 2752 (class 2604 OID 16442)
-- Name: job_stats id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.job_stats ALTER COLUMN id SET DEFAULT nextval('public.table_job_stats_id_seq'::regclass);


--
-- TOC entry 2753 (class 2604 OID 16496)
-- Name: job_status id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.job_status ALTER COLUMN id SET DEFAULT nextval('public.table_job_status_id_seq'::regclass);


--
-- TOC entry 2755 (class 2604 OID 16556)
-- Name: jobs id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jobs ALTER COLUMN id SET DEFAULT nextval('public.table_jobs_id_seq'::regclass);


--
-- TOC entry 2754 (class 2604 OID 16501)
-- Name: loan_accounts id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.loan_accounts ALTER COLUMN id SET DEFAULT nextval('public.table_loan_accounts_id_seq'::regclass);


--
-- TOC entry 2750 (class 2604 OID 16422)
-- Name: loan_status id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.loan_status ALTER COLUMN id SET DEFAULT nextval('public.table_loan_status_id_seq'::regclass);


--
-- TOC entry 2748 (class 2604 OID 16410)
-- Name: projects id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projects ALTER COLUMN id SET DEFAULT nextval('public.table_projects_id_seq'::regclass);


--
-- TOC entry 2747 (class 2604 OID 16404)
-- Name: regions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.regions ALTER COLUMN id SET DEFAULT nextval('public.table_regions_id_seq'::regclass);


--
-- TOC entry 2904 (class 0 OID 16425)
-- Dependencies: 214
-- Data for Name: borrowers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.borrowers (id, borrower_name, date_created, created_by, date_modified, modified_by) FROM stdin;
\.


--
-- TOC entry 2894 (class 0 OID 16395)
-- Dependencies: 204
-- Data for Name: countries; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.countries (id, country_code, country_name, region_id, date_created, created_by, date_modified, modified_by) FROM stdin;
\.


--
-- TOC entry 2907 (class 0 OID 16439)
-- Dependencies: 217
-- Data for Name: job_stats; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.job_stats (id, job_id, stat_desc, stat_value, date_created, created_by, date_modified, modified_by) FROM stdin;
\.


--
-- TOC entry 2910 (class 0 OID 16493)
-- Dependencies: 220
-- Data for Name: job_status; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.job_status (id, description, lable, value, date_created, created_by, date_modified, modified_by) FROM stdin;
2	JOB IS QUEUED	QUEUED	0	2020-01-14 11:39:34.53338	postgres	\N	\N
3	JOB IS RUNNING	RUNNING	1	2020-01-14 11:41:43.567309	postgres	\N	\N
4	JOB SUCCEEDED	SUCCEEDED	2	2020-01-14 11:41:43.567309	postgres	\N	\N
5	JOB HAS STOPPED	STOPPED	4	2020-01-14 11:41:43.567309	postgres	\N	\N
6	JOB FAILED	FAILED	3	2020-01-14 11:41:43.567309	postgres	\N	\N
\.


--
-- TOC entry 2913 (class 0 OID 16553)
-- Dependencies: 223
-- Data for Name: jobs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.jobs (id, job_name, job_status, start_time, end_time, file_name, date_created, created_by, date_modified, modified_by) FROM stdin;
3	IBRD_And_IDA_Statement_Data_Load	3	2020-01-15 06:13:52.167703	\N	ibrd-statement-of-loans-historical-data.csv	2020-01-15 06:13:52.167703	postgres	2020-01-15 06:13:52.167703	postgres
7	IBRD_And_IDA_Statement_Data_Load	3	2020-01-15 08:20:38.15627	\N	ibrd-statement-of-loans-historical-data.csv	2020-01-15 08:20:38.15627	postgres	2020-01-15 08:20:38.15627	postgres
8	IBRD_And_IDA_Statement_Data_Load	3	2020-01-15 08:23:15.364449	\N	ibrd-statement-of-loans-historical-data.csv	2020-01-15 08:23:15.364449	postgres	2020-01-15 08:23:15.364449	postgres
9	IBRD_And_IDA_Statement_Data_Load	3	2020-01-15 08:23:56.752321	\N	ibrd-statement-of-loans-historical-data.csv	2020-01-15 08:23:56.752321	postgres	2020-01-15 08:23:56.752321	postgres
10	IBRD_And_IDA_Statement_Data_Load	3	2020-01-15 08:25:03.192131	\N	ibrd-statement-of-loans-historical-data.csv	2020-01-15 08:25:03.192131	postgres	2020-01-15 08:25:03.192131	postgres
11	IBRD_And_IDA_Statement_Data_Load	3	2020-01-15 08:31:32.427573	\N	ibrd-statement-of-loans-historical-data.csv	2020-01-15 08:31:32.427573	postgres	2020-01-15 08:31:32.427573	postgres
16	IBRD_And_IDA_Statement_Data_Load	3	2020-01-15 08:38:37.48081	\N	ibrd-statement-of-loans-historical-data.csv	2020-01-15 08:38:37.48081	postgres	2020-01-15 08:38:37.48081	postgres
\.


--
-- TOC entry 2911 (class 0 OID 16498)
-- Dependencies: 221
-- Data for Name: loan_accounts; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.loan_accounts (id, job_id, loan_number, region_id, country_id, borrower_id, guarantor_country_id, loan_type_id, loan_status_id, interest_rate, currency_of_commitment, project_id, original_principal_amount, cancelled_amount, undisbursed_amount, disbursed_amount, repaid_to_ibrd, due_to_ibrd, exchange_adjustment, borrowers_obligation, sold_3rd_party, repaid_3rd_party, due_3rd_party, loans_held, first_repayment_date, last_repayment_date, agreement_signing_date, board_approval_date, effective_date_most_recent, closed_date_most_recent, last_disbursement_datet, date_created, created_by, date_modified, modified_by) FROM stdin;
\.


--
-- TOC entry 2902 (class 0 OID 16419)
-- Dependencies: 212
-- Data for Name: loan_status; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.loan_status (id, loan_status_desc, date_created, created_by, date_modified, modified_by) FROM stdin;
\.


--
-- TOC entry 2900 (class 0 OID 16413)
-- Dependencies: 210
-- Data for Name: loan_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.loan_type (id, loan_type_desc, date_created, created_by, date_modified, modified_by) FROM stdin;
\.


--
-- TOC entry 2898 (class 0 OID 16407)
-- Dependencies: 208
-- Data for Name: projects; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.projects (id, project_id, project_name, date_created, created_by, date_modified, modified_by) FROM stdin;
\.


--
-- TOC entry 2896 (class 0 OID 16401)
-- Dependencies: 206
-- Data for Name: regions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.regions (id, region_name, date_created, created_by, date_modified, modified_by) FROM stdin;
\.


--
-- TOC entry 2929 (class 0 OID 0)
-- Dependencies: 213
-- Name: table_borrowers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.table_borrowers_id_seq', 1, false);


--
-- TOC entry 2930 (class 0 OID 0)
-- Dependencies: 203
-- Name: table_countries_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.table_countries_id_seq', 1, false);


--
-- TOC entry 2931 (class 0 OID 0)
-- Dependencies: 216
-- Name: table_job_stats_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.table_job_stats_id_seq', 1, false);


--
-- TOC entry 2932 (class 0 OID 0)
-- Dependencies: 219
-- Name: table_job_status_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.table_job_status_id_seq', 6, true);


--
-- TOC entry 2933 (class 0 OID 0)
-- Dependencies: 222
-- Name: table_jobs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.table_jobs_id_seq', 18, true);


--
-- TOC entry 2934 (class 0 OID 0)
-- Dependencies: 218
-- Name: table_loan_accounts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.table_loan_accounts_id_seq', 1, false);


--
-- TOC entry 2935 (class 0 OID 0)
-- Dependencies: 215
-- Name: table_loan_stats_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.table_loan_stats_id_seq', 1, false);


--
-- TOC entry 2936 (class 0 OID 0)
-- Dependencies: 211
-- Name: table_loan_status_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.table_loan_status_id_seq', 1, false);


--
-- TOC entry 2937 (class 0 OID 0)
-- Dependencies: 209
-- Name: table_loan_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.table_loan_type_id_seq', 1, false);


--
-- TOC entry 2938 (class 0 OID 0)
-- Dependencies: 207
-- Name: table_projects_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.table_projects_id_seq', 1, false);


--
-- TOC entry 2939 (class 0 OID 0)
-- Dependencies: 205
-- Name: table_regions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.table_regions_id_seq', 1, false);


--
-- TOC entry 2764 (class 2620 OID 16497)
-- Name: job_status trig_job_status; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trig_job_status BEFORE INSERT OR UPDATE ON public.job_status FOR EACH ROW EXECUTE FUNCTION public.trig_logger();


--
-- TOC entry 2766 (class 2620 OID 16561)
-- Name: jobs trig_jobs; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trig_jobs BEFORE INSERT OR UPDATE ON public.jobs FOR EACH ROW EXECUTE FUNCTION public.trig_logger();


--
-- TOC entry 2765 (class 2620 OID 16502)
-- Name: loan_accounts trig_loan_accounts; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trig_loan_accounts BEFORE INSERT OR UPDATE ON public.loan_accounts FOR EACH ROW EXECUTE FUNCTION public.trig_logger();


--
-- TOC entry 2762 (class 2620 OID 16465)
-- Name: borrowers trig_logger_borrowers; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trig_logger_borrowers BEFORE INSERT OR UPDATE ON public.borrowers FOR EACH ROW EXECUTE FUNCTION public.trig_logger();


--
-- TOC entry 2757 (class 2620 OID 16466)
-- Name: countries trig_logger_countries; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trig_logger_countries BEFORE INSERT OR UPDATE ON public.countries FOR EACH ROW EXECUTE FUNCTION public.trig_logger();


--
-- TOC entry 2763 (class 2620 OID 16467)
-- Name: job_stats trig_logger_job_stats; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trig_logger_job_stats BEFORE INSERT OR UPDATE ON public.job_stats FOR EACH ROW EXECUTE FUNCTION public.trig_logger();


--
-- TOC entry 2761 (class 2620 OID 16470)
-- Name: loan_status trig_logger_loan_status; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trig_logger_loan_status BEFORE INSERT OR UPDATE ON public.loan_status FOR EACH ROW EXECUTE FUNCTION public.trig_logger();


--
-- TOC entry 2760 (class 2620 OID 16471)
-- Name: loan_type trig_logger_loan_type; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trig_logger_loan_type BEFORE INSERT OR UPDATE ON public.loan_type FOR EACH ROW EXECUTE FUNCTION public.trig_logger();


--
-- TOC entry 2759 (class 2620 OID 16472)
-- Name: projects trig_logger_projects; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trig_logger_projects BEFORE INSERT OR UPDATE ON public.projects FOR EACH ROW EXECUTE FUNCTION public.trig_logger();


--
-- TOC entry 2758 (class 2620 OID 16473)
-- Name: regions trig_logger_regions; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trig_logger_regions BEFORE INSERT OR UPDATE ON public.regions FOR EACH ROW EXECUTE FUNCTION public.trig_logger();


-- Completed on 2020-01-15 09:09:21

--
-- PostgreSQL database dump complete
--

