
import configparser
import os
import shutil

import numpy as np
import pandas as pd
from bokeh.io import output_file, output_notebook
from bokeh.layouts import column, gridplot, row
from bokeh.models import ColumnDataSource
from bokeh.models.widgets import Panel, Tabs
from bokeh.plotting import figure, show

from utilities import Email, File_Parser, db_conn, db_ops

try:
    conf_parser = configparser.ConfigParser()
    conf_parser.read_file(open('conf.cfg'))

    conf_parser.get('pg', 'host').strip()
    conf_parser.get('pg', 'username').strip()
    conf_parser.get('pg', 'password').strip()
    conf_parser.get('pg', 'port').strip()

    db_conn = db_conn(conf_parser.get('pg', 'host').strip(),
                      conf_parser.get('pg', 'username').strip(),
                      conf_parser.get('pg', 'password').strip(),
                      conf_parser.get('pg', 'database').strip(),
                      int(conf_parser.get('pg', 'port').strip())
                      )

    cursor = db_conn.conn.cursor()

    output_file('dataVisualization.html')
    job_name = 'IBRD_And_IDA_Statement_Data_Load'
    file_name = os.listdir('input')[0]

    new_job_id = cursor.execute(
        """insert into jobs(job_name, job_status, file_name) values(%s,1,%s) returning id""", (job_name, file_name))
    db_conn.conn.commit()

    job_id, = cursor.fetchone()
    csv_parser = File_Parser(cursor)
    db_ops = db_ops(csv_parser, cursor)
    csv_parser.load_lookups
    stats = {}

    df = pd.read_csv('input/%s' % file_name, parse_dates=['End of Period', 'First Repayment Date',	'Last Repayment Date',
                                                          'Agreement Signing Date',	'Board Approval Date',	'Effective Date (Most Recent)',	'Closed Date (Most Recent)',	'Last Disbursement Date'])

    for index, entry in df.iterrows():
        entry = entry.values
        csv_parser.parse_csv(entry, job_id)
        db_ops.create_insert_list(csv_parser.record)

    db_ops.load_to_database(', '.join(list(csv_parser.record.keys())),
                            ', '.join(csv_parser.table_values), db_ops.insert_lst, 'loan_accounts')

    db_conn.conn.commit()

    nullstats = df.isnull().sum()
    aggregates = df.describe()
    stats['TOTAL_RECORDS'] = df.sum()

    sample = df.sample(50)
    source = ColumnDataSource(nullstats)
    p = figure()
    p.hbar(source=source,
             size=10, color='green')

    p.title.text = 'Stats'
    p.xaxis.axis_label = 'Amount'
    p.yaxis.axis_label = 'Borrower'

    show(p)

    db_ops.load_stats(job_id, stats)

    sendmail = Email(conf_parser.get('email', 'port').strip(),
                     conf_parser.get('email', 'smtp_server').strip(),
                     conf_parser.get('email', 'sender_email').strip(),
                     conf_parser.get('email', 'password').strip())

    body = """
    Good day,

    The monthly IBRD_And_IDA_Statement_Data_Load file has been processed successfully and below are the statistics;

    """
    for each_stat in stats:
        body += """%s - %s \n""" % (stats, stats[each_stat])

    sendmail.message = body
    sendmail.send_email

    shutil.copyfile('input/%s' % file_name, 'file_store/%s' % file_name)
    os.remove('input/%s' % file_name)

    cursor.execute(
        """update jobs set job_status = 4 where id = %s""", (job_id,))
    db_conn.conn.commit()

    db_conn.conn.close()
except Exception as error:
    cursor.execute(
        """update jobs set job_status = 3 where id = %s""", (job_id,))
    db_conn.conn.commit()
    print(error)
